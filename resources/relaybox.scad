$fn=32;


difference() {
    union() {
        cube([70, 75, 22], center=true);    
        
        translate([0,0,-10])
        cube([77, 82, 2], center=true);
    }
    
    //space relay center
    translate([0,0,0.5])
    relay();
    
    //cable holes center
    translate([0,0,-4])
    rotate([90,0,0]) 
    cylinder(h=75, d=7, center=true);
    
    //space relay left
    translate([-22.5,0,0.5])
    relay();
    
    //cable holes left
    translate([-22.5,0,-4])
    rotate([90,0,0]) 
    cylinder(h=75, d=7, center=true);  
    
    //space relay right
    translate([22.5,0,0.5])
    relay();
    
    //cable holes right
    translate([22.5,0,-4])
    rotate([90,0,0]) 
    cylinder(h=75, d=7, center=true);  
    
    //reduce inner wall heigth
    translate([0,0,9])
    cube([66, 71, 5], center=true);
    
    //holes for screws
    translate([-36.5,39.0,0])
    cylinder(h=25, d=1, center=true);
    
    //holes for screws
    translate([36.5,39.0,0])
    cylinder(h=25, d=1, center=true);
    
    //holes for screws
    translate([36.5,-39.0,0])
    cylinder(h=25, d=1, center=true);
    
    //holes for screws
    translate([-36.5,-39.0,0])
    cylinder(h=25, d=1, center=true);
}





module relay() {
    cube([21,71,21], center=true);
}