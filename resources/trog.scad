$fn=100;
//rotate([180,0,0])
* union() {
difference() {
    union() {
        cylinder(h=60, r1=19, r2=80, center=true);
        translate([0,0,-28]) {
            difference([0,0,0]) {
                cube([40,90,55], center=true);
                translate([0,0,-25])
                    cube([40,90,10], center=true);
            }
        }

        //hole outside shape
        translate([0,-45,-50]) {
            rotate([-45,0,0])
                cylinder(h=40, r1=7, r2=12, center=true);
        }
        
        //close the smaller side hole of the screw
        translate([0,-46.5,-19])
            cube([40,3,58], center=true);
    }
    cylinder(h=60, r1=17, r2=78, center=true);    
    
    //smaller rotated cylinder to hold the screw
    translate([0,0,-24])
    rotate([90,0,0])
    cylinder(h=90, r=9.5, center=true);
    
    //bigger cylinder to push the screw in from one side
    //translated by 3mm to hold screw
    translate([0,3,-24])
    rotate([90,0,0])
    cylinder(h=90, r=16.5, center=true);
    
    //hole
   translate([0,-45,-50]) {
        rotate([-45,0,0])
            cylinder(h=70, r1=5, r2=10, center=true);
    }
    
    //screw hole left for motor
    translate([17.5,42.5,-32])
    rotate([90,0,0])
    color([1,0,0]) cylinder(h=5, r=0.5, center=true);

    //screw hole right for motor
    translate([-17.5,42.5,-32])
    rotate([90,0,0])
    color([1,0,0]) cylinder(h=5, r=0.5, center=true);

}



}


//closing lid right, print EXTRA (access for screw)
//close the smaller side hole of the screw
rotate([90,0,0]) //lay flat - deactivate for design
difference() {
translate([0,45.5,-27])
    cube([40,1.5,42], center=true);

translate([0,45.5,-32])
    rotate([90,0,0])
        intersection() {
            translate([0,17,0])
            cylinder(h=1.6, r=15, center=true);
            translate([0,-2,0])
            cylinder(h=1.6, r=15, center=true);
        }
  
        
    
//screw hole left for motor
translate([17.5,45.5,-32])
rotate([90,0,0])
color([1,0,0]) cylinder(h=3, r=1, center=true);

//screw hole right for motor
translate([-17.5,45.5,-32])
rotate([90,0,0])
color([1,0,0]) cylinder(h=3, r=1, center=true);
}

//just for designing //56.5
* translate([0,56.5,-32])
rotate([0,-90,-270])
color([0,1,0]) import("/Users/hansamann/Downloads/Step_28BYJ-48.stl");

