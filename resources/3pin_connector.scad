$fn = 50;

difference() {
    union() {
        translate([-13.5,0,4.4])
        cube([5,2,2], center=true);
        cube([35,7,7], center=true);
    }
    translate([13.5,0,2.45])
    cube([5.3,2.3,2.3], center=true);
    
    translate([0,0,1])
    magnet();
    //cylinder(h=10, d=0.75, center=true);
    
    translate([-6.5,0,1]) 
    magnet();
    cylinder(h=10, d=0.75, center=true);
    
    
    translate([6.5,0,1])
    magnet();
}
/*
difference() {
cube([10,10,10], center=true);
translate([0,0,2.5])
magnet();    
}*/



module magnet() {
    cube([5.2,5.2,5.1], center=true);
    cylinder(h=10, d=3, center=true);
}