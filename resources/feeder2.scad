$fn=100;

//top
* union() {
difference() {
    union() {
        difference() {
            sphere(50);
            sphere(48);
        }
    }
    //cut lower part of sphere
    translate([0,0,-25])
       cube([100, 100, 50], center=true);
    
    //a 45 deg opening
    translate([-25,25,25])
    difference() {
        cube([50,50,50], center=true);
        hypo = sqrt((50*50) + (50*50));
        translate([-25,-25,0])
        rotate([0,0,45]) cube([hypo,hypo,hypo], center=true);
    }
    
    //top cylinder opening
    translate([0,0,30])
    cylinder(h=52, r=10.2, center=true);
}

//2mm extra bottom to add top part to the buttom part
translate([0,0,-1])
    difference() {
        cylinder(h=2, r=50, center=true);
        cylinder(h=2, r=48, center=true);    
 
    }
}

//inner
* color([1,0,1]) 
difference() {
    union() {
        cylinder(h=4, r=47, center=true);

        rotate([0,90,0])
        cylinder(h=2, r=47, center=true);

        rotate([0,90,90])
        cylinder(h=2, r=47, center=true);

        rotate([0,90,45])
        cylinder(h=2, r=47, center=true);

        rotate([0,90,-45])
        cylinder(h=2, r=47, center=true);
       
        translate([0,0,47/2-0.5])
        cylinder(h=52, r1=10, r2=10, center=true);
    }

    //remove lower part
    translate([0,0,-47/2])
        cube([100,100,47], center=true);
    
    //remove nuts
    translate([0,0,5.3])
    scale([1.05,1.05,1.05]) //scale to make it fit better to scew
    nut();
    
}


 //motor
* translate([-8,0,1])
rotate([0,0,0])
color([0,1,0]) import("/Users/hansamann/go/src/gitlab.com/animalsensor/resources/Step_28BYJ-48.stl");


 //bottom 
difference() {
    cylinder(h=22, r=50, center=true);
    
    //~2mm top insert
    translate([0,0,10])
        difference() {
            cylinder(h=2, r=50, center=true);
            cylinder(h=2, r=47, center=true);    
     
        }
    
    color([0,1,0])
    translate([-8,0,1])
    scale([1.03,1.03,1.03])
    import("/Users/hansamann/go/src/gitlab.com/animalsensor/resources/Step_28BYJ-48.stl");
        
    //remove hole opening left
    translate([-8,18,10.3])
    cylinder(h=1.5, d=6, center=true);
    
    //remove hole opening right
    translate([-8,-18,10.3])
    cylinder(h=1.5, d=6, center=true);
        
   //remove a cylinder shape for cables
    translate([-25,0,3])
    rotate([0,80,0])
    scale([1,2,1])
    cylinder(h=55, r=5, center=true);  
        
    //space for cables
    translate([-23,0,9])
    cylinder(h=20, d=10, center=true);
     
    //save filament and remove a lot of the inner pla not required
    translate([0,0,-20 ])
    rotate_extrude(convexity = 10)
    translate([38, 0, 0])
    scale([1.0,1.5,1])
    circle(r = 12, $fn = 100);    
}


module nut() {
    difference() {
        union() {
            cylinder(h=8.5, d=5, center = true );
            translate([0,0,-5])
            cylinder(h=1.5, d=9, center = true);
        }

    //left
    translate([-2,0,1.25])
    cube([1.0,5,6], center=true);

    translate([2,0,1.25])
    cube([1.0,5,6], center=true);
    }
}

