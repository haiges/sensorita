$fn=100;


module pie_slice(r=10.0,a=30) {
  $fn=64;
  difference () {
      linear_extrude(height = 10)
      intersection() {
        circle(r=r);
        square(r);
        rotate(a-90) square(r);
      }
      cylinder(h=40, r=11, center=true);
  }
}


color([0,0,1]) 
rotate([0,0,1])
difference() {

    pie_slice(r=45,a=42);

    translate([2,1,1])
    scale([0.85,0.85,1])
    pie_slice(r=49,a=42);
}

//inner bottom
*color([1,0,1]) 
difference() {
    union() {
        translate([0,0,1.5])
        cylinder(h=3, r=47, center=true);

        //first wall
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //second wall
        rotate([0,0,90])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //third wall
        rotate([0,0,45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //fourth wall
        rotate([0,0,-45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
       
        //center stability cylinder
        translate([0,0,20])
        cylinder(h=40, r1=10, r2=10, center=true);
        
        //center top connecting element
        //.5 further down
        translate([0,0,44])
        cube([9,9,9], center=true);
    }
    
    //remove nuts
    translate([0,0,4])
    scale([1.05,1.05,1.05]) //scale to make it fit better to scew
    nut();    
}



module nut() {
    difference() {
        union() {
            cylinder(h=8.5, d=5, center = true );
            translate([0,0,-5])
            cylinder(h=1.5, d=9, center = true);
        }

    //left
    translate([-2,0,1.25])
    cube([1.0,5,6], center=true);

    translate([2,0,1.25])
    cube([1.0,5,6], center=true);
    }
}

