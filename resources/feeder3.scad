$fn=100;

//lid
rotate([0,180,0])
color([1,1,0])
translate([0,0,125])
difference() {
cylinder(h=10, r=53, center=true);
cylinder(h=10, r=11, center=true);
translate([0,0,-1])
cylinder(h=8, r=51, center=true);
}

//top
color([0.5,1,1]) 
translate([0,0,3])
union() {
    difference() {
        translate([0,0,60])
        union() {
            difference() {
                cylinder(h=130,r=50, center=true);
                cylinder(h=130,r=48, center=true);
            }
        }    
        //a 45 deg opening
        translate([-25,25,70])
        difference() {
            cube([50,50,110], center=true);
            hypo = sqrt((50*50) + (50*50));
            translate([-25,-25,0])
            rotate([0,0,45]) cube([hypo,hypo,110], center=true);
        }
        
        //top cylinder opening
        translate([0,0,30])
        cylinder(h=52, r=10.2, center=true);
    }
 
}

//inner bottom
color([1,0,1]) 
difference() {
    union() {
        translate([0,0,1.5])
        cylinder(h=3, r=47, center=true);

        //first wall
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //second wall
        rotate([0,0,90])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //third wall
        rotate([0,0,45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //fourth wall
        rotate([0,0,-45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
       
        //center stability cylinder
        translate([0,0,20])
        cylinder(h=40, r1=10, r2=10, center=true);
        
        //center top connecting element
        //.5 further down
        translate([0,0,44])
        cube([9,9,9], center=true);
    }
    
    //remove nuts
    translate([0,0,4])
    scale([1.05,1.05,1.05]) //scale to make it fit better to scew
    nut();    
}

//inner middle
color([1,0,0])
translate([0,0,40])
difference() {
    union() {
        translate([0,0,1.5])
        cylinder(h=3, r=47, center=true);

        //first wall
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //second wall
        rotate([0,0,90])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //third wall
        rotate([0,0,45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //fourth wall
        rotate([0,0,-45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
       
        //center stability cylinder
        translate([0,0,20])
        cylinder(h=40, r1=10, r2=10, center=true);
        
        //center top connecting element
        //.5 further down
        translate([0,0,44])
        cube([9,9,9], center=true);
        
    }
    
    //remove nuts
    translate([0,0,4.7])
    scale([1.05,1.05,1.05]) //scale to make it fit better to scew
    cube([9,9,9], center=true); 
}

 //inner top
color([0,1,1])
translate([0,0,80])
difference() {
    union() {
        translate([0,0,1.5])
        cylinder(h=3, r=47, center=true);

        //first wall
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //second wall
        rotate([0,0,90])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //third wall
        rotate([0,0,45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
        
        //fourth wall
        rotate([0,0,-45])
        translate([0,0,20])
        cube([47*2, 2, 40], center=true);
       
        
       
        //center stability cylinder
        translate([0,0,30])
        cylinder(h=60, r1=10, r2=10, center=true);
        
    }
    
    //remove nuts
    translate([0,0,4.7])
    scale([1.05,1.05,1.05]) //scale to make it fit better to scew
    cube([9,9,9], center=true);   
}



 //motor
* translate([-8,0,-10.5])
rotate([0,0,0])
color([0,1,0]) import("/Users/hansamann/go/src/gitlab.com/animalsensor/resources/Step_28BYJ-48.stl");


*//bottom 
translate([0,0,-11])
difference() {
    cylinder(h=22, r=50, center=true);
    
    //~2mm top insert
    translate([0,0,10])
        difference() {
            cylinder(h=2, r=50, center=true);
            cylinder(h=2, r=47, center=true);    
     
        }
    
    color([0,1,0])
    translate([-8,0,1])
    scale([1.03,1.03,1.03])
    import("/Users/hansamann/go/src/gitlab.com/animalsensor/resources/Step_28BYJ-48.stl");
        
    //remove hole opening left
    translate([-8,18,10.3])
    cylinder(h=1.5, d=6, center=true);
    
    //remove hole opening right
    translate([-8,-18,10.3])
    cylinder(h=1.5, d=6, center=true);
        
   //remove a cylinder shape for cables
    translate([-25,0,3])
    rotate([0,80,0])
    scale([1,2,1])
    cylinder(h=55, r=5, center=true);  
        
    //space for cables
    translate([-23,0,9])
    cylinder(h=20, d=10, center=true);
     
    //save filament and remove a lot of the inner pla not required
    translate([0,0,-20 ])
    rotate_extrude(convexity = 10)
    translate([38, 0, 0])
    scale([1.0,1.5,1])
    circle(r = 12, $fn = 100);    
}


module nut() {
    difference() {
        union() {
            cylinder(h=8.5, d=5, center = true );
            translate([0,0,-5])
            cylinder(h=1.5, d=9, center = true);
        }

    //left
    translate([-2,0,1.25])
    cube([1.0,5,6], center=true);

    translate([2,0,1.25])
    cube([1.0,5,6], center=true);
    }
}

