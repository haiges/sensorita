FROM golang:1.10-alpine3.7 AS build

ARG WDIR 

COPY . ${WDIR}

WORKDIR ${WDIR}

RUN go build -v main.go

FROM alpine:3.7

RUN apk add --no-cache tzdata
RUN ln -snf /usr/share/zoneinfo/Europe/Berlin /etc/localtime && echo "Europe/Berlin" > /etc/timezone

ARG WDIR

COPY --from=build ${WDIR}/main /app/main
COPY --from=build ${WDIR}/static /app/static
COPY --from=build ${WDIR}/templates /app/templates

EXPOSE 8080

WORKDIR /app

CMD ["/app/main"]