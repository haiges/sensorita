package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/kelvins/sunrisesunset"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/robfig/cron"
	"github.com/zsais/go-gin-prometheus"
	"image/color"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"periph.io/x/periph"
	"periph.io/x/periph/conn/gpio"
	"periph.io/x/periph/conn/gpio/gpioreg"
	"periph.io/x/periph/conn/i2c/i2creg"
	"periph.io/x/periph/conn/physic"
	"periph.io/x/periph/conn/spi/spireg"
	"periph.io/x/periph/devices/apa102"
	"periph.io/x/periph/devices/bmxx80"
	"periph.io/x/periph/host"
	"strconv"
	"strings"
	"time"
)

const (
	PIN_HALL        = "17" //BCM17 https://pinout.xyz/pinout/pin11_gpio17
	PIN_RELAY_LIGHT = "18" //BCM18 https://pinout.xyz/pinout/pin12_gpio18
	PIN_IN1 = "22"
	PIN_IN2 = "23"
	PIN_IN3 = "6"
	PIN_IN4 = "12"
)

var promTemperature prometheus.Gauge
var promHumidity prometheus.Gauge
var promPressure prometheus.Gauge
var promLight prometheus.Gauge
var promBrightness prometheus.Gauge
var promMotion prometheus.Gauge
var promIterations prometheus.Counter

var motionTimer *time.Timer

//env variables for container config
//all variables in this section can be changed by setting in-container environment variables
var SUBSYSTEM = "geckos"
var SHOW_ITERATIONS = "true" //string for web ui
var SHOW_LIGHT = "true"      //string for web ui
var VIDEO_URL = "https://cam.example.com"
var DELETE_FILES_MINUTES int64 = 60 * 24
var LIGHT_ON = "07:00"
var LIGHT_OFF = "19:00"
var NIGHTLIGHT_MINUTES int = 120

type SystemState struct {
	Temperature float64 `json:"temperature"`
	Humidity    float64 `json:"humidity"`
	Pressure    float64 `json:"pressure"`
	Iterations  float64 `json:"iterations"`
	Motion      bool    `json:"motion"`
	Light       bool    `json:"light"`
	Videos      []File  `json:"videos"`
}

type File struct {
	Name    string `json:"name"`
	Size    int64  `json:"size"`
	Changed int64  `json:"changed"`
}

var state SystemState
var periphHost *periph.State
var dev *bmxx80.Dev
var pinRelay gpio.PinIO
var leds *apa102.Dev
var pixels []color.NRGBA

var pinin1 gpio.PinIO
var pinin2 gpio.PinIO
var pinin3 gpio.PinIO
var pinin4 gpio.PinIO

var currentPhase Phase;
var phasePos int = 0

//full red as geckos cannot see red. lower green and blue, so it's not too bright for geckos
var NIGHTLIGHT_COLOR = color.NRGBA{R: 255, G: 100, B: 100}

type Phase struct { 
        In1 bool
        In2 bool
        In3 bool
        In4 bool
}

var phases = []Phase { 
        Phase {
            In1 : false,
            In2 : false,  
            In3: false,
            In4: true,
        },
        Phase {
            In1 : false,
            In2 : false,  
            In3: true,
            In4: true,
        },
        Phase {
            In1 : false,
            In2 : false,  
            In3: true,
            In4: false,
        },
        Phase {
            In1 : false,
            In2 : true,  
            In3: true,
            In4: false,
        },
        Phase {
            In1 : false,
            In2 : true,  
            In3: false,
            In4: false,
        },
        Phase {
            In1 : true,
            In2 : true,  
            In3: false,
            In4: false,
        },
        Phase {
            In1 : true,
            In2 : false,  
            In3: false,
            In4: false,
        },
        Phase {
            In1 : true,
            In2 : false,  
            In3: false,
            In4: true,
        },
}

func init() {

	if subsystem := os.Getenv("SUBSYSTEM"); subsystem != "" {
		log.Printf("Env variable SUBSYSTEM is set to '%v' and will override the default value.", subsystem)
		SUBSYSTEM = subsystem
	}

	if show_iterations := os.Getenv("SHOW_ITERATIONS"); show_iterations != "" {
		log.Printf("Env variable SHOW_ITERATIONS is set to '%v' and will override the default value.", show_iterations)
		SHOW_ITERATIONS = show_iterations
	}

	if show_light := os.Getenv("SHOW_LIGHT"); show_light != "" {
		log.Printf("Env variable SHOW_LIGHT is set to '%v' and will override the default value.", show_light)
		SHOW_LIGHT = show_light
	}

	if video_url := os.Getenv("VIDEO_URL"); video_url != "" {
		log.Printf("Env variable VIDEO_URL is set to '%v' and will override the default value.", video_url)
		VIDEO_URL = video_url
	}

	if delete_minutes := os.Getenv("DELETE_FILES_MINUTES"); delete_minutes != "" {
		log.Printf("Env variable DELETE_FILES_MINUTES is set to '%v' and will override the default value.", delete_minutes)
		DELETE_FILES_MINUTES, _ = strconv.ParseInt(delete_minutes, 10, 64)
	}

	if light_on := os.Getenv("LIGHT_ON"); light_on != "" {
		log.Printf("Env variable LIGHT_ON is set to '%v' and will override the default value.", light_on)
		LIGHT_ON = light_on
	}

	if light_off := os.Getenv("LIGHT_OFF"); light_off != "" {
		log.Printf("Env variable LIGHT_OFF is set to '%v' and will override the default value.", light_off)
		LIGHT_OFF = light_off
	}

	if nightlight_minutes := os.Getenv("NIGHTLIGHT_MINUTES"); nightlight_minutes != "" {
		log.Printf("Env variable NIGHTLIGHT_MINUTES is set to '%v' and will override the default value.", nightlight_minutes)
		NIGHTLIGHT_MINUTES, _ = strconv.Atoi(nightlight_minutes)
	}

	promTemperature = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "animals",
		Subsystem: SUBSYSTEM,
		Name:      "temperature",
		Help:      "Current temperature in Celsius.",
	})

	prometheus.MustRegister(promTemperature)

	promHumidity = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "animals",
		Subsystem: SUBSYSTEM,
		Name:      "humidity",
		Help:      "Current humidity in percent.",
	})

	prometheus.MustRegister(promHumidity)

	promPressure = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "animals",
		Subsystem: SUBSYSTEM,
		Name:      "pressure",
		Help:      "Current pressure in kPa",
	})

	prometheus.MustRegister(promPressure)

	promLight = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "animals",
		Subsystem: SUBSYSTEM,
		Name:      "light",
		Help:      "Current status of the light, 0 off | 1 on",
	})

	prometheus.MustRegister(promLight)

	promBrightness = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "animals",
		Subsystem: SUBSYSTEM,
		Name:      "brightness",
		Help:      "Current brightness of nightlight",
	})

	prometheus.MustRegister(promBrightness)

	promMotion = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "animals",
		Subsystem: SUBSYSTEM,
		Name:      "motion",
		Help:      "Current status of motion, 0 no motion | 1 motion",
	})

	prometheus.MustRegister(promMotion)

	promIterations = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "animals",
		Subsystem: SUBSYSTEM,
		Name:      "iterations",
		Help:      "Iterations over 24h, reset 9AM each day",
	})

	prometheus.MustRegister(promIterations)

	//led setup
	pixels = make([]color.NRGBA, 3)

	//physical setup
	opts := bmxx80.Opts{Temperature: bmxx80.O4x, Pressure: bmxx80.O4x, Humidity: bmxx80.O4x}

	var err error
	if periphHost, err = host.Init(); err != nil {
		log.Printf("Unable to init Periph.io Host - not running on a RPI? %v", err)
	} else {
		i, err := i2creg.Open("")
		if err == nil {
			dev, err = bmxx80.NewI2C(i, 0x76, &opts)
			if err == nil {
				log.Printf("Found %s", dev)
			}
		}

		if leds == nil {
			leds = getLEDs()
		}
	}

	//init state
	state = SystemState{0.0, 0.0, 0.0, 0.0, false, false, []File{}}
}

func main() {

	r := gin.Default()
	r.Delims("[[", "]]")
	prometheus := ginprometheus.NewPrometheus("gin")
	prometheus.Use(r)

	r.Static("/static", "./static")
	r.Static("/videos", "./videos")
	r.LoadHTMLGlob("templates/*")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{"SHOW_ITERATIONS": SHOW_ITERATIONS, "SHOW_LIGHT": SHOW_LIGHT, "VIDEO_URL": VIDEO_URL})
	})

	r.GET("/api/state", func(c *gin.Context) {
		c.JSON(http.StatusOK, state)
	})

	r.POST("/motion", func(c *gin.Context) {

		//if old timer present, kill it
		if motionTimer != nil {
			motionTimer.Stop()
			motionTimer = nil
		}

		motionTimer = time.AfterFunc(time.Second*60, func() {
			fmt.Println("Resetting motion metric to 0.")
			state.Motion = false
			promMotion.Set(0)
		})

		state.Motion = true
		promMotion.Set(1)
		c.JSON(http.StatusOK, gin.H{"success": true})

	})

	r.POST("/move/:steps", func(c *gin.Context) { 
		steps, err := strconv.Atoi(c.Param("steps"))
		if (err != nil) {
			log.Printf("Cannot convert to steps - %v", err)
			c.JSON(http.StatusOK, gin.H{"success": false, "error": err.Error()})
		} else {
			go moveStepper(steps)
			c.JSON(http.StatusOK, gin.H{"success": true})
		}
	})

	c := cron.New()
	c.AddFunc("@every 5s", update)
	c.AddFunc("0 0 9 * * *", resetIterations) //every day at 9 am
	c.Start()

	//start watching for HIGH changes for the iteration counter | hamster only
	//also setup relay pin
	//we use the bme280 to detect if we;re all good, a bit hacky.
	if dev != nil {
		// Lookup a pin by its number:
		p := gpioreg.ByName(PIN_HALL)
		fmt.Printf("%s: %s\n", p, p.Function())

		// Set it as input, with an internal pull down resistor:
		if err := p.In(gpio.PullDown, gpio.BothEdges); err != nil {
			log.Fatal(err)
		}

		// Wait for edges as detected by the hardware
		go func() {
			for {
				p.WaitForEdge(-1)
				if p.Read() == gpio.High {
					fmt.Println("Iterations++")
					state.Iterations++
					promIterations.Set(state.Iterations)
				}
			}
		}()

		//setup relay pin
		pinRelay = gpioreg.ByName(PIN_RELAY_LIGHT)
		fmt.Printf("%s: %s\n", p, p.Function())

		//setup motor pins
		pinin1 = gpioreg.ByName(PIN_IN1)
		pinin2 = gpioreg.ByName(PIN_IN2)
		pinin3 = gpioreg.ByName(PIN_IN3)
		pinin4 = gpioreg.ByName(PIN_IN4)
	}

	//start blocking web server
	r.Run() //:8080
}

func resetIterations() {
	log.Println("Resetting promIterations to 0")
	state.Iterations = 0
	promIterations.Set(state.Iterations)
}

func update() {
	lightOnOff := determineLight()
	fmt.Println("Light on? ", lightOnOff)

	if lightOnOff {
		if pinRelay != nil {
			pinRelay.Out(true)
		}
		state.Light = true
		promLight.Set(1)
	} else {
		if pinRelay != nil {
			pinRelay.Out(false)
		}
		state.Light = false
		promLight.Set(0)
	}

	//get temperature/humidity and update
	if dev != nil {
		env := physic.Env{}
		if err := dev.Sense(&env); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%8s %10s %9s\n", env.Temperature, env.Pressure, env.Humidity)

		humidityString := env.Humidity.String()
		temperatureString := env.Temperature.String()
		pressureString := env.Pressure.String()

		//log.Printf("%v %v %v", humidityString, temperatureString, pressureString)

		humidity, _ := strconv.ParseFloat(humidityString[:len(humidityString)-3], 64)
		state.Humidity = humidity
		log.Printf("Humidity: %v", state.Humidity)
		promHumidity.Set(state.Humidity)

		temperature, _ := strconv.ParseFloat(temperatureString[:len(temperatureString)-3], 64)
		state.Temperature = temperature
		log.Printf("Temperature: %v", state.Temperature)
		promTemperature.Set(state.Temperature)

		pressure, _ := strconv.ParseFloat(pressureString[:len(pressureString)-3], 64)
		state.Pressure = pressure
		log.Printf("Pressure: %v", state.Pressure)
		promPressure.Set(state.Pressure)

	} else {
		fmt.Println("No BME280 device initialized - skipping.")
	}

	//leds
	if leds != nil {
		brightness := nightlightBrightness()
		promBrightness.Set(brightness)
		currentColor := color.NRGBA{R: uint8(float64(NIGHTLIGHT_COLOR.R) * brightness), G: uint8(float64(NIGHTLIGHT_COLOR.G) * brightness), B: uint8(float64(NIGHTLIGHT_COLOR.B) * brightness)}
		setNightlight(currentColor)
	} else {
		fmt.Println("No APA102 device initialized - skipping.")
	}

	//update files
	files, err := ioutil.ReadDir("./videos")
	if err != nil {
		log.Fatal(err)
	}

	//deadline := time.Now().AddDate(0, 0, -1)
	deadline := time.Now().Add(-time.Minute * time.Duration(DELETE_FILES_MINUTES))
	videos := []File{}
	for _, f := range files {

		if f.ModTime().Before(deadline) { //older than deadline
			//delete
			err := os.Remove("./videos/" + f.Name())
			if err == nil {
				log.Printf("Deleted %v as it is older than one day.", f.Name())
			} else {
				log.Printf("Unable to delete %v", f.Name())
			}

		} else {
			//add to list
			log.Printf("Adding %v to videos list.", f.Name())
			video := File{Name: f.Name(), Size: f.Size(), Changed: f.ModTime().Unix()}
			videos = append(videos, video)
		}
	}

	state.Videos = videos

}

func nightlightBrightness() float64 {
	loc, _ := time.LoadLocation("Europe/Berlin")
	now := time.Now().In(loc)

	on, off, _ := getOnOffTimes()

	//time since off
	durationAfterSunset := now.Sub(off)
	log.Printf("Duration after offTime: %v", durationAfterSunset)

	//time till on
	durationTillSunrise := on.Sub(now)
	log.Printf("Duration till onTime: %+v", durationTillSunrise)

	limit := time.Minute * time.Duration(NIGHTLIGHT_MINUTES)
	percentageBrightness := float64(0)
	if durationAfterSunset > 0 && durationAfterSunset < limit {
		percentageBrightness = float64(1) - float64(durationAfterSunset)/float64(limit)
		if percentageBrightness < 0 {
			percentageBrightness = 0
		}
		log.Printf("within sunset limit - %v", percentageBrightness)

	} else if durationTillSunrise > 0 && durationTillSunrise < limit {
		percentageBrightness = float64(1) - float64(durationTillSunrise)/float64(limit)
		if percentageBrightness < 0 {
			percentageBrightness = 0
		}

		log.Printf("within sunrise limit - %v", percentageBrightness)
	}

	return percentageBrightness

}

func setNightlight(color color.NRGBA) {

	for i := 0; i < len(pixels); i++ {
		pixels[i] = color
	}

	_, err := leds.Write(apa102.ToRGB(pixels))

	if err != nil {
		log.Printf("Unable to write to SPI for leds: %v", err)
	} else {
		log.Printf("Nightlight set to %v", color)
	}

}

func determineLight() bool {
	loc, _ := time.LoadLocation("Europe/Berlin")
	now := time.Now().In(loc)

	on, off, _ := getOnOffTimes()
	return (now.After(on) && now.Before(off))
}

func getOnOffTimes() (time.Time, time.Time, error) {
	loc, _ := time.LoadLocation("Europe/Berlin")
	now := time.Now().In(loc)

	if LIGHT_ON != "" && LIGHT_OFF != "" {
		log.Printf("Using sunrise/sunset via ENV: %v %v", LIGHT_ON, LIGHT_OFF)

		onParts := strings.Split(LIGHT_ON, ":")
		offParts := strings.Split(LIGHT_OFF, ":")

		onHour, err := strconv.Atoi(onParts[0])
		if err != nil {
			return time.Time{}, time.Time{}, err
		}
		onMinute, err := strconv.Atoi(onParts[1])
		if err != nil {
			return time.Time{}, time.Time{}, err
		}

		offHour, err := strconv.Atoi(offParts[0])
		if err != nil {
			return time.Time{}, time.Time{}, err
		}
		offMinute, err := strconv.Atoi(offParts[1])
		if err != nil {
			return time.Time{}, time.Time{}, err
		}

		on := time.Date(now.Year(), now.Month(), now.Day(), onHour, onMinute, 0, 0, loc)
		off := time.Date(now.Year(), now.Month(), now.Day(), offHour, offMinute, 0, 0, loc)

		return on, off, nil
	} else {
		log.Printf("Calculating sunrise/sunset with lat long.")

		sunrise, sunset, err := sunrisesunset.GetSunriseSunset(48.171887, 11.353187, +2.0, now)
		// If no error has occurred, print the results
		if err == nil {
			fmt.Println("Now", now.Format("15:04"), "| Sunrise:", sunrise.Format("15:04"), "| Sunset:", sunset.Format("15:04")) // Sunrise: 06:11:44
		} else {
			fmt.Println(err)
		}

		sunriseHour, sunriseMinute, _ := sunrise.Clock()
		sunsetHour, sunsetMinute, _ := sunset.Clock()

		on := time.Date(now.Year(), now.Month(), now.Day(), sunriseHour, sunriseMinute, 0, 0, loc)
		off := time.Date(now.Year(), now.Month(), now.Day(), sunsetHour, sunsetMinute, 0, 0, loc)

		return on, off, nil

	}
}

func getLEDs() *apa102.Dev {
	s, err := spireg.Open("")
	if err != nil {
		log.Printf("Cannot open spireg, local dev? skipping spi init. %v", err)
		return nil
	}

	opts := apa102.DefaultOpts
	opts.NumPixels = 3
	//  opts.Intensity = 255

	d, err := apa102.New(s, &opts)
	if err != nil {
		log.Fatal(err)
	}
	return d
}

func moveStepper(steps int) {
    
    if (pinin1 != nil && pinin2 != nil && pinin3 != nil && pinin4 != nil) {

		log.Printf("Moving %v steps.", steps)
	    for i:=0; i < steps; i++{

	        currentPhase = phases[phasePos]
	        pinin1.Out(gpio.Level(currentPhase.In1))
	        pinin2.Out(gpio.Level(currentPhase.In2))
	        pinin3.Out(gpio.Level(currentPhase.In3))
	        pinin4.Out(gpio.Level(currentPhase.In4))
	        

	        phasePos++
	        if phasePos > 7 {
	            phasePos = 0
	        }

	        time.Sleep(15 * time.Millisecond)
	        i++
	    }

	    log.Printf("Done moving %v steps.", steps)
	} else {
		log.Printf("Not on a RPI - skipping.")

	}
}
