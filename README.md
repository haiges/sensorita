# Sensorita

Sensorita is the main project and written in golang. It is intended to run on a Rasperry PI as it makes use of the GPIO pins and sensors such as the BME280 for Temperature, Humidtiy and Pressure. You can start it on a laptop such as a MacBook though, it will just fail to initialize the hardware sensors. This allows you to modify the web ui for example without having to copy files between your laptop and the pi. 

To start sensorita locally on your laptop (for dev only), please first install `golang`. You may then run 

```
go run main.go
```

to start Sensorita on localhost:8080. This will of course not give you any hardware access.

## Configuration Options
The following settings can be changed via env variables. For running this project, you will typically want to install `docker` and then configure the env variables to fit your needs:

- SUBSYSTEM (default "geckos") - used for the /metrics endpoint and the name of the metrics. The default value will yield a metric name such as `animals_geckos_iterations` the the iteratiosn the hall sensor measured for example 
- SHOW_ITERATIONS (default "true") - whether or not to show the iterations metric (e.g. not required for a terrarium, so change to false)
- SHOW_LIGHT (default "true") - whether or not to show the light metric. This is the state of the PIN 18 which can be connected to a relay. It will be turned on after LIGHT_ON and off after LIGHT_OFF. 
- VIDEO_URL (default "https://cam.example.com") - The URL for the video stream which is shown within this web UI. You will need to change this.
- DELETE_FILES_MINUTES (default 60 * 24 = 1d) - Number of minutes after which video files recorded will be removed to free up space. 
- LIGHT_ON (default "07:00") - local (container local) time to switch on the relay. Note: this also defines the point in time the nightlight is completely off again. The nightlight switches on before LIGHT_ON and gradually increases intensity based on NIGHTLIGHT_MINUTES. 
- LIGHT_OFF (default "19:00") - local (container local) time to switch off the relay. Note: this also defines the point in time the nightlight starts to gradually turn off. The nightlight switches 100% after LIGHT_OFF and gradually decreases intensity based on NIGHTLIGHT_MINUTES. 
- NIGHTLIGHT_MINUTES (default 120) - the duration in minutes the nightlight will increase/decrease before/after LIGHT_ON/LIGHT_OFF.


## Starting Sensorita without Hall Sensor (.e.g. Geckos, Reptiles, Terrarium)

```
docker container run -d --network sensorita -p 9090:8080 -v /home/pi/motionout:/app/videos -v /sys:/sys --restart always --device /dev/gpiomem --device=/dev/i2c-1 --device=/dev/spidev0.0 --name sensorita -e SHOW_ITERATIONS='false' -e LIGHT_ON='07:00' -e LIGHT_OFF='19:00' -e NIGHTLIGHT_MINUTES='180' registry.gitlab.com/haiges/sensorita:1.8.0-arm
```

## Starting Sensorita with Hall Sensor (e.g. Hamsters)
```
docker container run -d --network sensorita -p 9090:8080 -v /home/pi/motionout:/app/videos -v /sys:/sys --restart always --device /dev/gpiomem --device=/dev/i2c-1 --name sensorita -e SHOW_LIGHT='false' -e VIDEO_URL='https://hamstercam1.flavor.de' -e SUBSYSTEM='hamster1' registry.gitlab.com/haiges/sensorita:1.8.0-arm
```

# Developer Hints

## SASS
The Web UI uses SASS to build the CSS in `css/mystyles.css`. Normally, no change is required, but if you need to change the CSS, you can choose to compile once or during longer dev have sass watch the styles and compile them on each change. 

### Compile once

```
sass sass/mystyles.scss:static/css/mystyles.css
```

### Watch for changes during dev

```
sass --watch sass/mystyles.scss:static/css/mystyles.css
```
