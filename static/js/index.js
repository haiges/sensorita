var app = new Vue({
    el: '#app',
    data: {
        temperature : 0.0,
        humidity: 0.0,
        pressure: 0.0,
        iterations : 0,
        motion : false,
        light : false,
        videos : []
    },
    computed : {
      showLight : function() {
        
        return document.body.dataset.showLight == 'true';
      },
      showIterations : function() {
        return document.body.dataset.showIterations == 'true';
      },
    },
    methods: {

    },
    created: function () { 
      vm = this;
      setInterval(function() { 
        getData()
        .then(data => {
          vm.temperature = data.temperature;
          vm.humidity = data.humidity;
          vm.pressure = data.pressure;
          vm.iterations = data.iterations;
          vm.motion = data.motion;
          vm.light = data.light;
          vm.videos = data.videos.map(video => {
            var date = new Date(video.changed*1000);
            var mb = video.size / 1024 / 1024;

            return {name:video.name, url:"/videos/"+video.name, size: mb.toFixed(2), changed:date.toLocaleString("de-de")};
          });
        })
        .catch(error => {
              console.log('Error: ', error);
              this.authenticated = false;
        });
      }, 1000)
    }


});

function getData() {
    
    return fetch("/api/state", {
        method: "GET", 
        mode: "cors", 
        cache: "no-cache"
    })
    .then(response => {
        if (response.ok)
            return response.json()
        else
            throw "Unable to get data.";
    });
}


//navbar burger activation
document.addEventListener('DOMContentLoaded', () => {

  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

});